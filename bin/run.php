<?php
#!/usr/bin/env php

require __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Console\Application;
use SagaProcessManager\Lib\Command\RequeueCommand;

$application = new Application();
$application->add(new RequeueCommand());
$application->run();