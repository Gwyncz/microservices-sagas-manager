#!/bin/sh

echo '#'
echo '# Registering service instance'
echo '#'

export PRIVATE_IP=$(ifconfig enp0s3 | grep 'netmask' | cut -d: -f2 | awk '{ print $2}');

curl -f --retry 7 --retry-delay 3 http://$PRIVATE_IP:8500/v1/agent/service/register -d "$(printf '{"ID":"saga-manager","Name":"saga-manager","Address":"%s","Port":9004, "Check":{"HTTP": "http://%s:9004/status", "Interval": "10s"}}' $PRIVATE_IP $PRIVATE_IP)"

curl -f --retry 7 --retry-delay 3 http://$PRIVATE_IP:8500/v1/agent/service/register -d "$(printf '{"ID":"saga-manager-mysql","Name":"saga-manager-mysql","Address":"%s","Port":6609, "Check":{"tcp": "%s:6609", "Interval": "10s"}}' $PRIVATE_IP $PRIVATE_IP)"


