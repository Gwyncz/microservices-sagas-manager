FROM alpine:3.6
MAINTAINER Jindrich Kubat <jindrich.kubat@outlook.com>

# Install packages
RUN apk --no-cache add php7 php7-fpm php7-mysqli php7-json php7-openssl php7-curl \
    php7-zlib php7-iconv php7-xml php7-phar php7-intl php7-dom php7-xmlreader php7-ctype php7-tokenizer\
    php7-mbstring php7-xmlwriter nginx supervisor curl

# Configure nginx
COPY docker/config/nginx.conf /etc/nginx/nginx.conf

# Configure PHP-FPM
COPY docker/config/fpm-pool.conf /etc/php7/php-fpm.d/zzz_custom.conf

# Configure supervisord
COPY docker/config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Add application
RUN mkdir -p /var/www/html
WORKDIR /var/www/html
COPY . /var/www/html/

ARG COMPOSER_VERSION=1.4.1
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
  && [ "$(curl -s https://composer.github.io/installer.sig)" = "$(php -r "echo hash_file('SHA384', 'composer-setup.php');")" ] || ( >&2 echo 'ERROR: Invalid installer signature'; exit 1 ) \
  && php composer-setup.php \
    --quiet \
    --filename=composer \
    --install-dir=/usr/local/bin \
    "--version=${COMPOSER_VERSION}" \
  && php -r "unlink('composer-setup.php');"


EXPOSE 80

WORKDIR /var/www/html
RUN mkdir -p logs && chmod -R 777 logs

RUN composer install --no-progress --no-interaction
RUN composer dump-autoload --no-ansi --no-interaction --optimize

CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
