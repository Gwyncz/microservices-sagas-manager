<?php

namespace SagaProcessManager\Lib;

use Zack\Saga\SagaInterface;
use Monolog\Logger;

class PurchaseSaga implements SagaInterface
{
	/**
	 * @var Logger
	 */
	private $logger;

	/**
	 * @var PurchaseSubscriber
	 */
	private $subscriber;

	/**
	 * PurchaseSaga constructor.
	 *
	 * @param Logger $logger
	 * @param PurchaseSubscriber $subscriber
	 * @param \Zipkin $zipkin
	 */
	public function __construct(Logger $logger, PurchaseSubscriber $subscriber)
	{
		$this->logger = $logger;
		$this->subscriber = $subscriber;
	}

	/**
	 * @return \Generator
	 */
	public function run(): \Generator
	{
		// Wait for the 'purchase.created' event.
		$event = yield take('purchase.created');

		$this->logger->debug('Purchase Saga Start', [
			'e' => 'saga-purchase',
			'event' => 'Starting purchase saga',
            'data' => $event
		]);

		if ($event->getStatus() != PurchaseEvent::STATUS_PURCHASE_SAVED) {
		    yield dispatch('purchase.set_to_problem', $event);
			return;
		}

		//fire new event for delete cart
		yield dispatch('cart.delete', $event);
		if ($event->getStatus() != PurchaseEvent::STATUS_CART_DELETED) {
			//revert all transactions
			yield dispatch('cart.revert', $event);
			yield dispatch('purchase.set_to_problem', $event);
			return;
		}

		yield dispatch('saga.complete', $event);

		$this->logger->debug('Purchase Saga End', [
			'e' => 'saga-purchase',
			'event' => 'Ending saga purchase',
            'data' => $event
		]);
	}
}