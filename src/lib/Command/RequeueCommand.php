<?php

namespace SagaProcessManager\Lib\Command;

use SagaProcessManager\Lib\PurchaseEvent;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\Environment;

class RequeueCommand extends Command
{
	protected function configure()
	{
		$this
			// the name of the command (the part after "bin/console")
			->setName('app:requeue-sagas')
			// the short description shown while running "php bin/console list"
			->setDescription('Requeue unfinished sagas')
			// the full command description shown when running the command with
			// the "--help" option
			->setHelp('Command for requeuing all unfinished sagas');
	}

	protected function execute(InputInterface $input, OutputInterface $output)
	{

		// Create a mock environment for testing with
		$environment = Environment::mock(
			[
				'REQUEST_METHOD' => 'POST',
				'REQUEST_URI' => '/purchase'
			]
		);

		// Set up a response object
		$response = new Response();

		// Use the application settings
		$settings = require __DIR__ . '/../../settings.php';

		// Instantiate the application
		$app = new App($settings);

		// Set up dependencies
		require __DIR__ . '/../../dependencies.php';

		// Register routes
		require __DIR__ . '/../../routes.php';

		//load data from database
		$db = $app->getContainer()->get('db');
		$stmt = $db->prepare('SELECT saga.purchase_id, rp.payload
 							  FROM saga
 							  JOIN request_payload AS rp ON saga.purchase_id = rp.purchase_id 
 							  WHERE saga.status = ?
 							  AND saga.purchase_id NOT IN (
 							  	SELECT purchase_id FROM saga WHERE status = ?
 							  )
 						 ');

		$statusEnd = PurchaseEvent::STATUS_CART_DELETED;
		$statusBegin = PurchaseEvent::STATUS_PURCHASE_SAVED;

		$stmt->bind_param("dd", $statusBegin, $statusEnd);
		$stmt->execute();

		/* bind result variables */
		$stmt->bind_result($purchaseId, $payload);

		/* fetch values */
		$dbPayload = [];
		while ($stmt->fetch()) {
			//get payload
			$dbPayload[$purchaseId] = unserialize(base64_decode($payload));
		}
		$stmt->close();

		foreach ($dbPayload as $pId => $requestData) {
			// Set up a request object based on the environment
			$request = Request::createFromEnvironment($environment);
			$request = $request->withParsedBody($requestData);

			// Process the application
			$response = $app->process($request, $response);
			printf("Purchase %s has been requeued with result: %s\n", $pId, (string)$response->getBody());
		}
	}
}