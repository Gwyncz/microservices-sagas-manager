<?php

namespace SagaProcessManager\Lib;

use Symfony\Component\EventDispatcher\Event;

class PurchaseEvent extends Event
{

	const STATUS_PURCHASE_SAVED = 1;
	const STATUS_PURCHASE_SET_TO_PROBLEM = 2;
	const STATUS_CART_DELETED = 3;
	const STATUS_CART_REVERTED = 4;

	private $id;

	private $cart = [];

	private $customer = [];

	private $status;

	public function __construct(string $id, array $cart, array $customer)
	{
		$this->id = $id;
		$this->cart = $cart;
		$this->customer = $customer;
	}

	/**
	 * @return array
	 */
	public function getCart(): array
	{
		return $this->cart;
	}

	/**
	 * @param array $cart
	 */
	public function setCart(array $cart)
	{
		$this->cart = $cart;
	}

	/**
	 * @return array
	 */
	public function getCustomer(): array
	{
		return $this->customer;
	}

	/**
	 * @param array $customer
	 */
	public function setCustomer(array $customer)
	{
		$this->customer = $customer;
	}

	/**
	 * @return string
	 */
	public function getId(): string
	{
		return $this->id;
	}

	/**
	 * @param string $id
	 */
	public function setId(string $id)
	{
		$this->id = $id;
	}

	/**
	 * @return mixed
	 */
	public function getStatus()
	{
		return $this->status;
	}

	/**
	 * @param mixed $status
	 */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getValues()
    {
        return [
            'id' => $this->getId(),
            'status'=> $this->getStatus()
         ];
    }

}