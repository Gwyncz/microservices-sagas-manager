<?php
/**
 * Created by PhpStorm.
 * User: jkubat
 * Date: 18.06.2018
 * Time: 21:22
 */

namespace SagaProcessManager\Lib;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Monolog\Logger;

class PurchaseDbSubscriber implements EventSubscriberInterface
{
	/**
	 * @var \mysqli
	 */
	private $db;

	/**
	 * @var Logger
	 */
	private $logger;

	/**
	 * PurchaseDbSubscriber constructor.
	 *
	 * @param \mysqli $db
	 * @param Logger $logger
	 */
	public function __construct(\mysqli $db, Logger $logger)
	{
		$this->db = $db;
		$this->logger = $logger;
	}

	/**
	 * @param Event $event
	 */
	public function logPurchase(Event $event) {

		$stmt = $this->db->prepare('SELECT 1 FROM saga WHERE purchase_id = ? AND status = ?');

		$id = $event->getId();
		$status = $event->getStatus();

		$stmt->bind_param("sd", $id , $status);
		$stmt->execute();
		$stmt->bind_result($exists);
		$stmt->fetch();
		$stmt->close();

		if(!$exists) {
			$stmt = $this->db->prepare('INSERT INTO saga (purchase_id, status) VALUES (?, ?)');
			$stmt->bind_param('sd', $id, $status);
			$stmt->execute();
			$stmt->close();
		}
	}

	/**
	 * @param string $purchaseId
	 * @param string $payload
	 */
	public function saveRequestPayload(string $purchaseId, string $payload) {
		$stmt = $this->db->prepare('SELECT 1 FROM request_payload WHERE purchase_id = ?');

		$stmt->bind_param("s", $purchaseId);
		$stmt->execute();
		$stmt->bind_result($exists);
		$stmt->fetch();
		$stmt->close();

		if(!$exists) {
			$sql = 'INSERT INTO request_payload (purchase_id, payload) VALUES (?,?)';
			$stmt = $this->db->prepare($sql);
            $enPayload = base64_encode($payload);
			$stmt->bind_param('ss', $purchaseId, $enPayload);
			$stmt->execute();
			$stmt->close();
		}
	}

	/**
	 * @param Event $event
	 */
	public function deletePayload(Event $event) {
		$sql = 'DELETE FROM request_payload WHERE purchase_id = ?';
		$stmt = $this->db->prepare($sql);

		$id = $event->getId();
		$stmt->bind_param('s', $id);
		$stmt->execute();
		$stmt->close();
	}

	/**
	 * @return array
	 */
	public static function getSubscribedEvents()
	{
		return [
			'purchase.created' => [
				['logPurchase', 99],
			],
			'purchase.set_to_problem' => [
				['logPurchase', 89]
			],
			'cart.delete' => [
				'logPurchase', 79
			],
			'cart.revert' => [
				'logPurchase', 69
			],
			'saga.complete' => [
				'deletePayload', 50
			],
		];
	}
}