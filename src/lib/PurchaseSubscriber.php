<?php

namespace SagaProcessManager\Lib;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Monolog\Logger;
use GuzzleHttp;
use Zipkin\Timestamp;
use Zipkin\Propagation\Map;

class PurchaseSubscriber implements EventSubscriberInterface
{
	/**
	 * @var Logger
	 */
	private $logger;
	/**
	 * @var GuzzleHttp\Client
	 */
	private $purchaseClient;

	/**
	 * @var GuzzleHttp\Client
	 */
	private $cartClient;

	/**
	 * @var
	 */
	private $authToken;

	/**
	 * @var \Zipkin
	 */
	private $zipkin;

	/**
	 * PurchaseSaga constructor.
	 *
	 * @param Logger $logger
	 * @param GuzzleHttp\Client $purchaseClient
	 * @param GuzzleHttp\Client $cartClient
	 */
	public function __construct(Logger $logger, GuzzleHttp\Client $purchaseClient, GuzzleHttp\Client $cartClient, \Zipkin $zipkin, string $authToken)
	{
		$this->logger = $logger;
		$this->purchaseClient = $purchaseClient;
		$this->cartClient = $cartClient;
		$this->authToken = $authToken;
		$this->zipkin = $zipkin;
	}

	/**
	 * @param Event $event
	 * @return bool
	 */
	public function savePurchase(Event $event)
	{
		try {

            //trace the purchase
			$tracing = $this->zipkin->getTracing();
			$tracer = $tracing->getTracer();
			$rootSpan = $this->zipkin->getRootSpan();
			$childSpan = $this->zipkin->getChildSpan($tracer, $rootSpan->getContext(), 'save-purchase ' . (string)$event->getId(), 'saga::save-purchase' );

			//pass headers to the microservice
			$headers = ['Authorization' => $this->authToken];
			$injector = $tracing->getPropagation()->getInjector(new Map());
			$injector($childSpan->getContext(), $headers);

			$res = $this->purchaseClient->put('save/' . $event->getId(), [
				'json' => [
					'cart' => $event->getCart(),
					'customer' => $event->getCustomer()
				],
				'headers' => $headers
			]);

			if ($res->getStatusCode() != 201) {
                throw new \Exception('Purchase revert unexpected status' . $res->getStatusCode());
			}

			$event->setStatus(PurchaseEvent::STATUS_PURCHASE_SAVED);

			$childSpan->tag(\Zipkin\Tags\HTTP_STATUS_CODE, $res->getStatusCode());
			$this->logger->info('Purchase saga - save purchase success',[
				'e' => 'saga-purchase-detail',
				'event' => 'Transaction method - Save purchase OK',
                'eventData' => $event->getValues(),
                'status' => $res->getStatusCode(),
			]);


		} catch (\Exception $e) {

            //revert purchase
            $this->logger->addError(
                'Purchase saga error',[
                'e' => 'saga-purchase-detail',
                'event' => 'Transaction method - Save purchase error',
                'eventData' => $event->getValues(),
                'exception'=> $e->getMessage()
            ]);

			if (isset($res)) {
				$childSpan->tag(\Zipkin\Tags\HTTP_STATUS_CODE, $res->getStatusCode());
			}
			$childSpan->annotate($e->getMessage(), Timestamp\now());
			//finish tracing
		}

		$childSpan->finish();
		$childSpan->flush();
	}

	/**
	 * @param Event $event
	 * @return bool
	 */
	public function setPurchaseToProblem(Event $event)
	{
		try {

			$headers = ['Authorization' => $this->authToken];
			$res = $this->purchaseClient->put('problem/' . $event->getId(),['headers' => $headers]);

			if ($res->getStatusCode() != 200) {
				throw new \Exception('Purchase revert unexpected status' . $res->getStatusCode());
			}
		} catch (\Exception $e) {

            //revert purchase
            $this->logger->addError(
                'Purchase saga error',[
                'e' => 'saga-purchase-detail',
                'event' => 'Compensation method - Set Purchase to Problem error',
                'eventData' => $event->getValues(),
                'exception'=> $e->getMessage()
            ]);


			return;
		}

		$event->setStatus(PurchaseEvent::STATUS_PURCHASE_SET_TO_PROBLEM);
        $this->logger->addInfo(
            'Purchase saga - set purchase to trouble success',[
            'e' => 'saga-purchase-detail',
            'event' => 'Compensation method - Set Purchase to Problem ok',
            'eventData' => $event->getValues(),
            'status'=> $res->getStatusCode()
        ]);
	}

	/**
	 * @param Event $event
	 */
	public function deleteCart(Event $event) {
		try {

			//trace the purchase
			$tracing = $this->zipkin->getTracing();
			$tracer = $tracing->getTracer();
			$rootSpan = $this->zipkin->getRootSpan();
			$childSpan = $this->zipkin->getChildSpan($tracer, $rootSpan->getContext(), 'delete-cart of purchase ' . (string)$event->getId(), 'saga::delete-cart' );

			//pass headers to the microservice
			$headers = ['Authorization' => $this->authToken];
			$injector = $tracing->getPropagation()->getInjector(new Map());
			$injector($childSpan->getContext(), $headers);

			$headers = ['Authorization' => $this->authToken];
			$res = $this->cartClient->delete('del/' . $event->getCustomer()['id'],['headers' => $headers]);

			if ($res->getStatusCode() != 200) {
			    throw new \Exception('Purchase revert unexpected status' . $res->getStatusCode());
			}

			$event->setStatus(PurchaseEvent::STATUS_CART_DELETED);

            $this->logger->addInfo(
                'Purchase saga - delete cart success',[
                'e' => 'saga-purchase-detail',
                'event' => 'Transaction method - delete cart ok',
                'eventData' => $event->getValues(),
                'status'=> $res->getStatusCode()
            ]);


            $childSpan->tag(\Zipkin\Tags\HTTP_STATUS_CODE, $res->getStatusCode());

		} catch (\Exception $e) {

		    $statusCode = 500;
			if (isset($res)) {
                $statusCode = $res->getStatusCode();
			    $childSpan->tag(\Zipkin\Tags\HTTP_STATUS_CODE, $statusCode);
			}

            //revert purchase
            $this->logger->addError(
                'Purchase saga error',[
                'e' => 'saga-purchase-detail',
                'event' => 'Transaction method - Delete cart error',
                'eventData' => $event->getValues(),
                'status'=> $statusCode,
                'exception'=> $e->getMessage()
            ]);
		}

		$childSpan->finish();
		$childSpan->flush();
	}

	/**
	 * @param Event $event
	 */
	public function revertCart(Event $event) {
		try {

			$headers = ['Authorization' => $this->authToken];
			foreach ($event->getCart() as $product) {

				$res = $this->cartClient->put('set/' . $event->getCustomer()['id'],[
					'json' => [
						'product' => $product['id'],
						'amount' => $product['amount']
					],'headers' => $headers
				]);

				if ($res->getStatusCode() != 201) {
					throw new \Exception('Purchase revert unexpected status' . $res->getStatusCode());
				}
			}

            $this->logger->addInfo(
                'Purchase saga - revert cart success',[
                'e' => 'saga-purchase-detail',
                'event' => 'Compensation method - Revert cart ok',
                'eventData' => $event->getValues(),
            ]);

		} catch (\Exception $e) {

            //revert purchase
            $this->logger->addError(
                'Purchase saga error',[
                'e' => 'saga-purchase-detail',
                'event' => 'Compensation method - Revert cart error',
                'eventData' => $event->getValues(),
                'status'=> $res->getStatusCode(),
                'exception'=> $e->getMessage()
            ]);


			return;
		}

		$event->setStatus(PurchaseEvent::STATUS_CART_REVERTED);
	}

	/**
	 * @return array
	 */
	public static function getSubscribedEvents()
	{
		return [
			'purchase.created' => [
				['savePurchase', 100]
			],
			'purchase.set_to_problem' => [
				['setPurchaseToProblem', 90]
			],
			'cart.delete' => [
				'deleteCart', 80
			],
			'cart.revert' => [
				'revertCart', 70
			],
		];
	}
}