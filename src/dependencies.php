<?php
// DIC configuration

$container = $app->getContainer();

// monolog
$container['logger'] = function ($c) {
	$settings = $c->get('settings')['logger'];
	$logger = new Monolog\Logger($settings['name']);
	$logger->pushProcessor(new Monolog\Processor\UidProcessor());
	$handler = new Monolog\Handler\StreamHandler($settings['path'], $settings['level']);
	$handler->setFormatter(new Monolog\Formatter\JsonFormatter());
	$logger->pushHandler($handler);
	return $logger;
};
//Purchase Client
$container['purchaseClient'] = function ($c) {
	$url = $c->get('settings')['purchase-service-url'];
	return new GuzzleHttp\Client(['base_uri' => $url,'timeout' => 5]);
};

//Cart Client
$container['cartClient'] = function ($c) {
	$url = $c->get('settings')['cart-service-url'];
	return new GuzzleHttp\Client(['base_uri' => $url,'timeout' => 5]);
};


$container['zipkin'] = function ($c) {
	$settings = $c->get('settings')['zipkin'];
	return new Zipkin($settings['url'], $settings['port']);
};

$container['db'] = function ($c) {
	$settings = $c->get('settings')['mysql'];
	$db = new mysqli($settings['host'], 'root', $settings['pass'], $settings['db'], $settings['port']);
	if(mysqli_connect_errno()) {
		throw new Exception('Cannot connect to the database');
	}
	return $db;
};

