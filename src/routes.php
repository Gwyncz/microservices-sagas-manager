<?php
use Symfony\Component\EventDispatcher\EventDispatcher;
use Zack\Saga\Processor;
use SagaProcessManager\Lib\PurchaseSaga;
use SagaProcessManager\Lib\PurchaseSubscriber;
use SagaProcessManager\Lib\PurchaseDbSubscriber;
use SagaProcessManager\Lib\PurchaseEvent;
use Zipkin\Timestamp;


require_once 'Zipkin.php';


//insert or modify product in the cart
$app->post('/purchase', function ($request, $response, $args) {

	/**
	 * ENABLE ZIPKIN TRACING
	 */
	$tracing = $this->zipkin->getTracing();
	$extractedContext = $this->zipkin->getRequestContext($request);
	$tracer = $tracing->getTracer();
	$rootSpan = $this->zipkin->getNewRootSpan($tracer, 'saga-process-manager', $extractedContext);

	/**
	 * PROCESS THE REQUEST
	 */
	$body = $request->getParsedBody();
	if (!$body['purchase'] && is_int($body['purchase'])) {
		$err = 'Purchase ID is not valid';
		$rootSpan->anotate($err);
		$this->zipkin->flush($tracer, $rootSpan);
		return $response->withJson(['error' => $err], 400);
	}

	//create the event dispatcher
	$eventDispatcher = new EventDispatcher();
	$processor = Processor::create($eventDispatcher);

	//class for processing events
	$purchaseSubscriber = new PurchaseSubscriber($this->logger, $this->purchaseClient, $this->cartClient, $this->zipkin, $this->settings['auth-token']);
	$purchaseDbSubscriber = new PurchaseDbSubscriber($this->db, $this->logger);

	//save the request payload
	$purchaseDbSubscriber->saveRequestPayload($body['purchase'], serialize($request->getParsedBody()));

	//init the purchase saga
	$processor->run(new PurchaseSaga($this->logger,$purchaseSubscriber));
	$eventDispatcher->addSubscriber($purchaseSubscriber);

	//init event sourcing
	$eventDispatcher->addSubscriber($purchaseDbSubscriber);

	//dispatch the first event
	$event = new PurchaseEvent($body['purchase'], $body['cart'], $body['customer']);
	$eventDispatcher->dispatch('purchase.created', $event);

	//check the status in the end
	if ($event->getStatus() == PurchaseEvent::STATUS_CART_DELETED) {
		$rootSpan->annotate('Saga OK', Timestamp\now());
		$this->zipkin->flush($tracer, [$rootSpan]);
		return $response->withJson(['status' => 'OK']);
	}

	/**
	 * finish the zipking tracing
	 */
	$rootSpan->annotate('Saga FAILED', Timestamp\now());
	$this->zipkin->flush($tracer, [$rootSpan]);
	return $response->withJson(['status' => 'FAILED'], 409);
});

$app->get('/status', function($request, $response, $args) {
	return $response->withJson(['status' => 'OK']);
});