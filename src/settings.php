<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => false, // Allow the web server to send the content-length header
	    'purchase-service-url' => $_SERVER['PURCHASE_SERVICE_URL'] ?? '',
	    'cart-service-url' => $_SERVER['CART_SERVICE_URL'] ?? '',
	    'auth-token' => '5ae8bc71ff0fec14616c6ae33784f264d03b489abcd0c151567b4d38',

        // Monolog settings
        'logger' => [
            'name' => 'saga-process-manager',
            'path' => __DIR__ . '/../logs/saga-process-manager-log.json',
            'level' => \Monolog\Logger::DEBUG,
        ],

	    //Mysql settings
	    'mysql' => [
		    'host' => 'saga-service-mysql',
		    'port' => '3306',
		    'pass' => 'Ck7jmJ9gLTsWUVui',
		    'db'   => 'saga'
	    ],

	    //zipkin
	    'zipkin' => [
		    'url' => 'http://zipkin.service.consul',
		    'port' => 9411
	    ]
    ],
];
