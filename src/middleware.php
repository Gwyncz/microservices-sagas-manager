<?php
// Application middleware
$app->add(function ($request, $response, $next) {

	$start = microtime(true);
	$response = $next($request, $response);

	$uri = $request->getUri();
	$this->logger->info('ProductService request', [
		'e' => 'request',
		'request' => ['method' => $request->getMethod(),
			'url' => (string)$uri->getBaseUrl() . (string)$uri->getPath(),
			'query' => (string) $uri->getQuery()
		],
		'request-data' => $request->getParsedBody(),
		'statusCode' => $response->getStatusCode(),
		'time' => microtime(true) - $start
	]);

	return $response;
});