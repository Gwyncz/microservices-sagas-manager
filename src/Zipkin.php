<?php
/**
 * Created by PhpStorm.
 * User: jkubat
 * Date: 09.03.2018
 * Time: 14:31
 */

use Zipkin\Endpoint;
use Zipkin\Propagation\TraceContext;
use Zipkin\Span;
use Zipkin\Tracer;
use Zipkin\Tracing;
use Zipkin\Samplers\BinarySampler;
use Zipkin\TracingBuilder;
use Zipkin\Propagation\SamplingFlags;
use Zipkin\Timestamp;
use Zipkin\Propagation\Map;

class Zipkin
{
	const SERVICE_NAME = 'saga-process-manager';
	const ZIPKIN_ENDPOINT = '%s:%d/api/v2/spans';

	/**
	 * @var string
	 */
	private $endpointUrl;

	/**
	 * @var Tracing
	 */
	private $tracing;

	/**
	 * @var Span
	 */
	private $rootSpan;

	/**
	 * Zipkin constructor.
	 *
	 * @param string $url
	 * @param int $port
	 */
	public function __construct(string $url, int $port)
	{
		$this->endpointUrl = sprintf(self::ZIPKIN_ENDPOINT, $url, $port);
	}

	public function getTracing() : Tracing {

		if ($this->tracing != null) {
			return $this->tracing;
		}

		$endpoint = Endpoint::create(self::SERVICE_NAME);

		$reporter = new \Zipkin\Reporters\Http(\Zipkin\Reporters\Http\CurlFactory::create(), ['endpoint_url' => $this->endpointUrl]);
		$sampler = BinarySampler::createAsAlwaysSample();
		$tracing = TracingBuilder::create()
			->havingLocalEndpoint($endpoint)
			->havingSampler($sampler)
			->havingReporter($reporter)
			->build();

		$this->tracing = $tracing;
		return $tracing;
	}

	/**
	 * @param Tracer $tracer
	 * @param string $name
	 * @return Span
	 */
	public function getNewRootSpan(Tracer $tracer, string $name, SamplingFlags $extractedContext) : Span {

		$rootSpan = $tracer->nextSpan($extractedContext);
		$rootSpan->start();
		$rootSpan->start(Timestamp\now());
		$rootSpan->setName($name);
		$rootSpan->setKind(\Zipkin\Kind\SERVER);

		$this->rootSpan = $rootSpan;
		return $rootSpan;
	}

	/**
	 * @return Span
	 */
	public function getRootSpan() {
		return $this->rootSpan;
	}

	/**
	 * @param Tracer $tracer
	 * @param TraceContext $context
	 * @param string $tag
	 * @param string $name
	 * @return Span
	 */
	public function getChildSpan(Tracer $tracer, TraceContext $context, string $tag, string $name) {

		$childSpan = $tracer->newChild($context);
		$childSpan->setKind(\Zipkin\Kind\SERVER);
		$childSpan->tag(\Zipkin\Tags\HTTP_PATH, $tag);
		$childSpan->start();
		$childSpan->setName($name);

		return $childSpan;
	}

	/**
	 * @param Tracer $tracer
	 * @param Span $rootSpan
	 */
	public function flush(Tracer $tracer, array $spans) {
		if (@get_headers($this->endpointUrl)) {
			foreach ($spans as $span) {
				$span->finish();
			}
			$tracer->flush();
		}
	}

	/**
	 * @param $request
	 */
	public function getRequestContext($request) : SamplingFlags {

		$carrier = [];

		if (!empty($request->getHeader('x-b3-traceid'))) {
			$carrier = [
				'x-b3-traceid' => $request->getHeader('x-b3-traceid')[0],
				'x-b3-spanid' => $request->getHeader('x-b3-spanid')[0],
				'x-b3-parentspanid' => $request->getHeader('x-b3-parentspanid')[0],
				'x-b3-sampled' => $request->getHeader('x-b3-sampled')[0],
				'x-b3-flags' => $request->getHeader('x-b3-flags')[0],
			];
		}

		/* Extracts the context from the HTTP headers */
		$extractor = $this->tracing->getPropagation()->getExtractor(new Map());
		$extractedContext = $extractor($carrier);
		return $extractedContext;
	}
}