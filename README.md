# Demo of a Saga proces manager based on the Slim Framework 3

This demo demonstrates basic implementation of saga proces manager in PHP. Using the Symphony event system. 

Demo contains handler /purchase which calls two endpoints (not part of the demo)

## Install the Application

Remove the composer.lock and run composer install

Install docker

Run docker-compose up -d

Login to the mysql container and create database structure from docker/mysql.init

Request example:

{
  "purchase":"59cab3283f39e3", 
	"cart":[
      {
         "id":1,
         "price":20,
         "amount":1,
         "totalPrice":20
      }
   ],
   "customer":{
      "name":"Name Surname",
      "address":"Street 123",
      "city":"city",
      "email":"email@email.cz",
      "phone":"123456789"
   }
}
